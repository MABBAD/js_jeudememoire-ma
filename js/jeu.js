/* eslint-disable no-undef */
/**
 * initialisation des cartes et de leurs positions
 * ainsi que le temps et les paires trouvees
 */

/**
 * le jeu ne marche pas avec les cookie car il faut mettre sur un serveur local
 * donc a chaque reload le meilleur score disparait (il revient a zero)
 */

Terrain.isRunning = false
Terrain.meilleurScore = 0

// en cas de clique 2 fois sur le btn partie
Terrain.clickDeuxFois = false
// -----------------------------------------------------------------------------------------------------------------------------------

// affichage du terrain sans pouvoir jouer
let terrainCartes = new Terrain()
terrainCartes.genererTerrain()

// -----------------------------------------------------------------------------------------------------------------------------------

// gerer le commencement de la partie et son arret en cliquant sur le bouton nouvelle partie
const boutonPartie = document.getElementById('button-partie')
boutonPartie.addEventListener('click', function () {
    // soit on commence une nouvelle partie
    if (Terrain.isRunning === false && !Terrain.clickDeuxFois) {
        boutonPartie.innerText = 'Arreter'

        Terrain.clickDeuxFois = !Terrain.clickDeuxFois

        // generer le terrain
        terrainCartes = new Terrain()
        terrainCartes.genererTerrain()
        terrainCartes.afficherCartesPourUnLapsDeTemps()

        // on lance le timer apres un laps de temps
        Terrain.tempsPourLancerLaPartie = window.setTimeout(function () {
            Terrain.isRunning = true
            Terrain.tempsPartie = window.setInterval(Terrain.lancerTimer, 100)
        }, 2200)

    // soit on arrete la partie
    } else {
        boutonPartie.innerText = 'Nouvelle Partie'

        Terrain.clickDeuxFois = !Terrain.clickDeuxFois

        // annuler le timer
        clearTimeout(Terrain.tempsPourLancerLaPartie)

        // annuler le timer
        Terrain.isRunning = false
        window.clearInterval(Terrain.tempsPartie)
    }
})

// -----------------------------------------------------------------------------------------------------------------------------------

// gerer le son du jeu, la musique et le son de fin de partie
let musicPlaying = false
const musicGame = document.getElementById('musique-jeu')
musicGame.volume = 0.5
// alterner entre mute et non mute
const hautParleur = document.getElementById('haut-parleur')
hautParleur.addEventListener('click', function () {
    if (!musicPlaying) {
        hautParleur.src = 'images/haut_parleur_on.png'
        musicGame.play()
        musicGame.muted = false
        musicPlaying = true
    } else {
        hautParleur.src = 'images/haut_parleur_off.png'
        musicGame.muted = true
        musicPlaying = false
    }
})
